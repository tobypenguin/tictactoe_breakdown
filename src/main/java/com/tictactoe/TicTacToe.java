/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tictactoe;

import java.util.Scanner;

/**
 *
 * @author TUFGaming
 */
public class TicTacToe {

    static String field[][]
            = {{"-", "-", "-"},
            {"-", "-", "-"},
            {"-", "-", "-"}};
    static Scanner kb = new Scanner(System.in);
    static String player = "X";
    static boolean isFinish = false;
    static String winner = "-";
    static int row = 0;
    static int col = 0;
    static int round = 0;
    static boolean win = false;

    static void showWelcome() {
        System.out.println("Welcom to OX Game.");
    }

    static void showTable() {
        System.out.println(" 1 2 3");
        int num = 1;
        for (int i = 0; i < 3; i++) {
            System.out.print(num + " ");
            for (int k = 0; k < 3; k++) {
                System.out.print(field[i][k] + " ");
            }
            System.out.println();
            num++;
        }
    }

    static void showTurn() {
        System.out.println(player + " turn.");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (field[row][col].equals("-")) {
                field[row][col] = player;
                break;
            }
            System.out.println("ERROR. You need to enter" + player + " again.");
        }
    }

    static void checkWin() {
        checkDraw();
        checkCol();
        checkRow();
        checkDiagonal();
        round++;
    }

    static void checkRow() {
        for (int i = 0; i < 3; i++) {
            if (field[i][0] == player && field[i][1] == player
                    && field[i][2] == player) {
                winner = player;
                win = true;
                isFinish = true;
                break;
            }
        }
    }

    static void checkCol() {
        for (int i = 0; i < 3; i++) {

            if (field[0][i] == player && field[1][i] == player
                    && field[2][i] == player) {
                winner = player;
                win = true;
                isFinish = true;
                break;
            }
        }

    }

    static void checkDiagonal() {
        if (((field[0][0] == player && field[1][1] == player
                && field[2][2] == player)
                || (field[0][2] == player && field[1][1] == player
                && field[2][0] == player))) {
            winner = player;
            win = true;
            isFinish = true;
        }
    }

    static boolean checkDraw() {
        if (round == 9) {
            return true;
        }
        return false;
    }

    static void switchPlayer() {
        if (player == "X") {
            player = "O";
        } else {
            player = "X";
        }
    }

    static void showResult() {
        System.out.println("Player " + player + " Win.");
    }

    static void showBye() {
        System.out.println("Bye bye.");
    }

    public static void main(String[] args) {
        try {
            showWelcome();
            do {
                showTable();
                showTurn();
                input();
                checkWin();
                if (checkDraw()) {
                    showTable();
                    System.out.println("Draw.");
                    break;
                }
                switchPlayer();
            } while (!isFinish);
            if (!checkDraw()) {
                showTable();
                showResult();
            }
            showBye();
        } catch (java.util.InputMismatchException E) {
            System.out.println("Error input mismatch.");
        } catch (java.lang.ArrayIndexOutOfBoundsException E) {
            System.out.println("Error must input number 1 to 3.");

        }
    }
}
